| **Temp Measurement**                       | Range       | Resolution   | Accuracy                                  |
| :-----------                               | :-----:     | :----------: | :--------:                                |
| Flue Temperature                           | 0-600&deg;C | 0.1&deg;C    | &plusmn;2.0&deg;C<br>&plusmn;0.3% reading |
| Inlet Temperature<br>(Internal sensor)     | 0-50&deg;C  | 0.1&deg;C    | &plusmn;1.0&deg;C<br>&plusmn;0.3% reading |
| Inlet Temperature<br>(External sensor)<br> | 0-600&deg;C | 0.1&deg;C    | &plusmn;2.0&deg;C<br>&plusmn;0.3% reading |

---

| **Flue Gas Measurement**                | Range                                                       | Resolution   | Accuracy                                          |
| :-----------                            | :-----:                                                     | :----------: | :--------:                                        |
| Oxygen<sup>\*2</sup>                    | 0-21%                                                       | 0.1%         | &plusmn;0.3%                                      |
| Carbon Monoxide<sup>\*1</sup>           | 0-20ppm<br>21-2,000ppm<br>Above 2000ppm Purge pump operates | 1ppm         | &plusmn;3ppm<br>&plusmn;5% reading<br>unspecified |
| Carbon dioxide<sup>\*1</sup>            | 0-20%                                                       | 0.1%         | &plusmn;0.3% volume                               |
| Efficiency (Net or Gross)<sup>\*2</sup> | 0-99.9%                                                     | 0.1%         | &plusmn;1.0% reading                              |
| Efficiency High (C)<sup>\*2</sup>       | 0-119.9%                                                    | 0.1%         | &plusmn;1.0% reading                              |
| Excess Air<sup>\*2</sup>                | 0-250%                                                      | 0.1%         | &plusmn;0.2% reading                              |
| CO/CO<sub>2</sub> ratio<sup>\*2</sup>   | 0-0.999                                                     | 0.0001%      | &plusmn;5% reading                                |

<small>_<sup>\*1</sup> Using dry gases at STP_</small>  
<small>_<sup>\*2</sup> Calculated_</small>

**Carbon Dioxide resolution is 0.01% below 1% measured value.**

---

| **Pressure (differential)** | Range      | Resolution         | Accuracy          |                   
|:--------   | :-----:            | :-----:           | :-------:         |
| Nominal range &plusmn;80mbar<br>Maximum over range without damage to sensor is &plusmn;400mbar | &plusmn;0.2mbar<br>&plusmn;1mbar<br>&plusmn;80mbar | Maximum<br>0.001mbar<br>&lt;25mbar | &plusmn;0.005mbar<br>&plusmn;0.03mbar<br>&plusmn;3% of reading |

---

|                           |                                                                                                              |
| :--------                 | :-----                                                                                                       |
| **Pre-programmed Fuels**  | Natural gas Propane, Butane, LPG, Light Oils (28/35 sec), Wood Pellets, Town Gas, Coke Gas, Bio Oil, Bio Gas |
| **User programmed Fuels** | 5 user defined fuels                                                                                         |

---

|                      |        |
| :--------            | :----- |
| **Storage Capacity** | 60 Combustion tests<br>20 Pressure & Temperature tests<br>20 Tightness tests<br>20 Temperature tests<br>20 Room CO tests<br>20 Room CO tests<br>20 Commissioning tests | 

---

|                       |                                                                                        |
| :--------                   | :------                                                                                      |
| **Ambient Operating Range** | 0&deg;C to &plusmn;45&deg;C 10% to 90% RH non-condensing                                     |
| **Battery Type / Life**     | 4AA cells<br>&gt;8 hours using Alkaline AA cells                                             |
| **Chargers (optional)**     | 100-240v charger, for NiMH batteries only<br>12v in vehicle charger, for NiMH batteries only |

---

| Dimensions | &nbsp;                                                                                                                         |
| :--------  | :-----                                                                                                                         |
| Weight     | 0.8kg handset with protective rubber cover                                                                                     |
| Handset    | 200 x 45 x 90mm                                                                                                                |
| Probe      | 301mm long including handle<br>6mm diameter x 240mm long stainless steel shaft with 2m long neoprene hose. Type K thermocouple |
